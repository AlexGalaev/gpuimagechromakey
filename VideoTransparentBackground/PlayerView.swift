//
//  PlayerView.swift
//  VideoTransparentBackground
//
//  Created by Alexey Galaev on 11/9/18.
//  Copyright © Expanse. All rights reserved.
//

import UIKit
import GPUImage
import AVFoundation

class PlayerView: GPUImageView {
	
	var movie: GPUImageMovie!
	var filter: GPUImageChromaKeyBlendFilter!
	var sourcePicture: GPUImagePicture!
	var firstImage: GPUImagePicture!
	var currentFilename: String?
	
	var isPlaying = false
	
	override func awakeFromNib() {
		super.awakeFromNib()
		customInit()
	}
	
	func customInit() {
		let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(togglePlay))
		addGestureRecognizer(gestureRecognizer)
		backgroundColor = UIColor.clear
	}
	
	func configure(file: String) {
		guard let url = Bundle.main.url(forResource: file, withExtension: "mp4") else { return }
	
		filter = GPUImageChromaKeyBlendFilter()
		filter.setColorToReplaceRed(0.000, green: 1, blue:0.000)
		//filter.smoothing = 0.00
		//filter.thresholdSensitivity = 0.000

		if let cgImage = thumbnailImage(url: url) {
			let scale = 1.0 / UIScreen.main.scale
			setConstraint(constant: CGFloat(cgImage.width) * scale, attribute: .width)
			setConstraint(constant: CGFloat(cgImage.height) * scale, attribute: .height)
		}
		
		movie = GPUImageMovie(url: url)
		movie.playAtActualSpeed = true
		movie.addTarget(filter)
		movie.shouldRepeat = true
		
		let backgroundImage = UIImage(named: "transparent.png")
		sourcePicture = GPUImagePicture(image: backgroundImage, smoothlyScaleOutput: true)!
		sourcePicture.addTarget(filter)
		sourcePicture.processImage()
		
		filter.addTarget(self)
	}
	
	@objc func togglePlay() {
		guard !isPlaying else {
			movie.endProcessing()
			isPlaying = false
			return
		}
		
		movie.startProcessing()
		isPlaying = true
	}

	
	private func thumbnailImage(url: URL) -> CGImage? {
		let asset = AVURLAsset(url: url)
		let assetGenerator = AVAssetImageGenerator(asset: asset)
		assetGenerator.appliesPreferredTrackTransform = true
		assetGenerator.apertureMode = AVAssetImageGenerator.ApertureMode.encodedPixels
		let time = CMTimeMakeWithSeconds(1.0, preferredTimescale: 1)
		var actualTime: CMTime = CMTimeMake(value: 0, timescale: 0)
		let cgImage = try? assetGenerator.copyCGImage(at: time, actualTime: &actualTime)
		return cgImage
	}
	
	// MARK: - Constraints
	
	private func getConstraint(attribute: NSLayoutConstraint.Attribute) -> NSLayoutConstraint? {
		return constraints.first(where: {$0.identifier == "attribute\(attribute.rawValue)"})
	}
	
	private func setConstraint(constant: CGFloat, attribute: NSLayoutConstraint.Attribute) {
		if let constraint = getConstraint(attribute: attribute) {
			constraint.constant = constant
		} else {
			let constraint = NSLayoutConstraint(item: self, attribute: attribute, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: constant)
			constraint.priority = UILayoutPriority(rawValue: 999)
			constraint.identifier = "attribute\(attribute.rawValue)"
			addConstraint(constraint)
		}
	}
}
