//
//  PlayerViewController.swift
//  
//  Created by Alexey Galaev on 11/9/18.
//  Copyright © Expanse. All rights reserved.
//  

import UIKit
import Foundation
class PlayerViewController: UIViewController {
	
	let blackBGVideo = "green-back"
	let chromakeyBG = "Single"
	
	@IBOutlet weak var videoView: PlayerView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		videoView.configure(file: chromakeyBG)
	}
}
